from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm
from tasks.models import Task
from projects.models import Project

# Create your views here.
@login_required
def list_projects(request):
    list_projects = Project.objects.filter(owner=request.user)
    context = {"list_projects": list_projects}
    return render(request, "projects/list_projects.html", context)


def show_project(request, id):
    show_project = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project=show_project)

    context = {
        "task_object": tasks,
        "project_name": show_project.name,
        "project_description": show_project.description,
    }
    return render(request, "projects/show_project.html", context)


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("show_project")
    else:
        form = CreateTaskForm()

    context = {"create_task": form}

    return render(request, "projects/create_task.html", context)


def show_my_tasks(request):
    show_my_tasks = Task.objects.filter(assignee=request.user)

    context = {"my_tasks": show_my_tasks}

    return render(request, "projects/show_my_tasks.html", context)
